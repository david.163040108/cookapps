// import Logo from "../../assets/images/logo.png";
class SearchBar extends HTMLElement {

    // constructor() {
    //     super();
    //     this.shadowDOM = this.attachShadow({mode: "open"});
    // }

    connectedCallback(){
        this.render();
    }

    set clickEvent(event) {
        this._clickEvent = event;
        this.render();
    }


    get value() {
        return this.querySelector("#searchElement").value;
    }

    render() {
        this.innerHTML = `
        <div class="container">
            <div class="input-group mb-3">
                <input type="search" id="searchElement" class="form-control" placeholder="Cari Makanan Kesukaan Mu" aria-label="Recipient's username" aria-describedby="button-addon2">
                <div class="input-group-append">
                  <button class="btn btn-outline-light bg-success" type="submit" id="searchButtonElement">Cari Makanan</button>
                </div>
            </div>
        </div>
       `;

        this.querySelector("#searchButtonElement").addEventListener("click", this._clickEvent);
    }
}

customElements.define("search-bar", SearchBar);