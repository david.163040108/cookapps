class MealItem extends HTMLElement {

    // constructor() {
    //     super();
    //     this.shadowDOM = this.attachShadow({mode: "open"});
    // }

    set meal(meal) {
        this._meal = meal;
        this.render();
    }

    render() {
        this.className = "col-xl-3 col-md-6 mb-4";
        this.innerHTML = `
      
            <div class="card border-0 shadow">
                    <img src="${this._meal.strMealThumb}" class="card-img-top" alt="...">
                    <div class="card-body text-center">
                    <h5 class="card-title mb-0">${this._meal.strMeal}</h5>
                    <div class="card-text text-black-50">${this._meal.strCategory}</div>
                    </div> 
            </div> 
    `;
    }
}

customElements.define("meal-item", MealItem);