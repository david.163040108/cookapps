import './meal-item.js';

class MealList extends HTMLElement {

    // constructor() {
    //     super();
    //     this.shadowDOM = this.attachShadow({mode: "open"});
    // }

    set meals(meals) {
        this._meals = meals;
        this.render();
    }

    render() {
        this.innerHTML = "";
        this.className = "row";
        this._meals.forEach(meal => {
            const mealItemElement = document.createElement("meal-item");
            mealItemElement.meal = meal;
            this.appendChild(mealItemElement);
        })
    }

    renderError(message) {
        this.innerHTML = `
        <style>
             .placeholder {
                   font-weight: lighter;
                   color: rgba(0,0,0,0.5);
                   -webkit-user-select: none;
                   -moz-user-select: none;
                   -ms-user-select: none;
                   user-select: none;
                   text-align:center;
               }
           </style>`;
        this.innerHTML += `<h1 class="placeholder text-center">${message}</h1>`;
    }
}

customElements.define("meal-list", MealList);