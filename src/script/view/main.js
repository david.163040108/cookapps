import '../component/search-bar.js';
import '../component/meal-list.js';
import {
 DataSource,
 Main
} from '../data/data-source.js';

const search = () => {
    const searchElement = document.querySelector("search-bar");
    const mealListElement = document.querySelector("meal-list");

    const onButtonSearchClicked = async () => {
        try{
            const result = await DataSource.searchClub(searchElement.value);
            renderResult(result);
        } catch (message) {
            fallbackResult(message)
        }
    };

    const renderResult =  results => {
        mealListElement.meals = results;
    };

    const fallbackResult = message => {
        mealListElement.renderError(message);
    };

    searchElement.clickEvent = onButtonSearchClicked;
};

const main = () => {
    const mealListElement = document.querySelector("meal-list");

    const ViewMain= async () => {
        try{
            const result = await Main.DataList();
            renderResult(result);
        } catch (message) {
            fallbackResult(message)
        }
    };

    const renderResult =  results => {
        mealListElement.meals = results;
    };

    const fallbackResult = message => {
        mealListElement.renderError(message);
    };

    ViewMain();
}
export {
    main,
    search  
}; 